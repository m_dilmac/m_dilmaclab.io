---
date: "2016-06-11T16:55:21+03:00"
categories: "Güvenlik"
title: "Target Blank ile Birlikte Gelen Window Opener Problemi"
layout: post
---

<p>Her halde ilk &ouml;ğrendiğimiz html etiketlerinden biri <strong>a</strong> etiketidir. Bu etiket sayesinde bir metine link verebiliyoruz, eğer linkin yeni sekmede a&ccedil;ılmasını istiyorsak a etiketimize <strong>target=&quot;_blank&quot;</strong> eklemesi yaparak yeni sekmede istediğimiz linki a&ccedil;tırabiliyoruz.&nbsp;Aslında bu kullanım olduk&ccedil;a masum bir kullanım yolu, fakat işin aslında bir istismara izin veren bir y&ouml;ntem bu yol. Yeni sekmede sayfayı a&ccedil;tırmak i&ccedil;in kullanmış olduğumuz target=&quot;_blank&quot; yeni sekmeyi a&ccedil;arken <strong>window.opener</strong>&nbsp;objesini oluşturmaktadır, bu objemiz i&ccedil;inde kaynak sekmeyi kalıtımsal olarak barındırmaktadır. Barındırmış olduğu bu sekme üzerinde ise javascript ile işlemler yapmanıza izin vermekte. Şimdi dilerseniz basit bir &ouml;rneği ş&ouml;yle bir gif ile g&ouml;sterelim;</p>

<figure>
  <img src="/assets/target-blank-ile-birlikte-gelen-window-opener-problemi/1.gif" alt="Örnek bir saldırı" />
  <figcaption>Örnek bir saldırı</figcaption>
</figure>

<p>Gif'de çok basit bir şekilde location ile yönlendirme yaptık buradan bir phishing saldırısı yapılabilir eğer bu yöntem sizi kesmediyse biraz daha işi ciddileştirelim ve daha derine inip window.opener.document ile kaynak sekmede ki document objesine erişerek üzerinde değişiklik yaparak sayfaya bir javascript kodu yerleştirelim ve input alanlarına girilen değerleri kayıt edelim ve post işlemi gerçekleştirilirken bu değerleri kendimize gönderelim;</p>

<p>Gif'de çok basit bir şekilde location ile yönlendirme yaptık buradan bir phishing saldırısı yapılabilir veya daha derine inip isterseniz window.opener.document ile kaynak sekmede ki document objesine erişerek üzerinde değişiklik yapabilirsiniz kısacası gerisi sizin hayalinize kalıyor :) Bu duruma çözüm olarak ise her _blank ile oluşturulan yeni sayfada opener objesinin temizlenmesi gerekli. Bu işlem için ise a etiketine <strong>rel="noopener noreferrer"</strong> özelliğini vermeniz yeterli olacaktır.</p>

<p>Bu yazıyı bir süredir yazmayı düşünüyordum fakat bir türlü üşengeçliğimden yazamadım :) <a href="http://ademilter.com/" target="_blank" rel="noopener noreferrer">Adem İlter</a> tarafından başlatılan <a href="http://salyangoz.me/" target="_blank" rel="noopener noreferrer">salyangoz.me</a> projesine bakarken bir deniyeyim dedim ve bildirdim durumu sağolsun Adem abi benide <a href="http://salyangoz.me/humans.txt" target="_blank" rel="noopener noreferrer">humans.txt</a>'ye teşekkür listesine ekledi :) Salyangoz projesine ait gif ise şurada ki gibidir;</p>

<figure>
  <img src="/assets/target-blank-ile-birlikte-gelen-window-opener-problemi/2.gif" alt="Salyangoz.me Projesinde Bulunan Window Opener Problemi" />
  <figcaption>Salyangoz.me Projesinde Bulunan Window Opener Problemi</figcaption>
</figure>


<p><strong>Dipnot:</strong></p>
<ol>
<li>Bu zafiyetten bana sohbet arasında bahseden <a href="https://gokmenguresci.com" target="_blank" rel="noopener noreferrer">Gökmen Güreşçi</a>'ye teşekkür ederim.</li>
<li>Farklı çözüm yollarıda bulunmaktadır ben sadece en basit yolunu yazmak istedim :)</li>
</ol>


<p>Gif'de bulunan kodlar ise şöyle;</p>
<script src="https://gist.github.com/MuhammetDilmac/761e2e0a689acbba0702d64c9d08c2ad.js"></script>
